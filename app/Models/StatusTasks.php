<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusTasks extends Model
{
    public $fillable = [
        'status',
    ];
}
