<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'slug'=>$this->slug,
            'description'=>$this->description,
            'created_at'=>Carbon::parse($this->create_at)->format('Y-m-d'),
            'updated_at'=>Carbon::parse($this->update_at)->format('Y-m-d'),
        ];
    }
}
