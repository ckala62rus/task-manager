<?php

namespace App\Http\Resources;

use App\Models\StatusTasks;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = StatusTasks::where('id', $this->status_id)->first();
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'status_id'=>$this->status_id,
            'status' => $status->status,
            'project_id'=>$this->project_id,
            'created_at'=>Carbon::parse($this->create_at)->format('Y-m-d H:i:s'),
            'updated_at'=>Carbon::parse($this->update_at)->format('Y-m-d H:i:s'),
        ];
    }
}
