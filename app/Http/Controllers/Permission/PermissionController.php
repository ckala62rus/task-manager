<?php

namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Permission\CreatePermissionRequest;
use App\Http\Resources\PermissionsResource;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Управление правами для ролей.
 * Добавление, удаление, синхронизация,
 * получение списка ролей, получение списка разрешений
 * Class PermissionController
 * @package App\Http\Controllers\Permission
 */
class PermissionController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        if ($user->hasRole('admin')){
            return response('you admin', 200);
        }
        if ($user->hasRole('user')){
            return response('you user', 200);
        }

        return response()->json('access denied');
        //?role_id=1&permission_ids=1
    }

    /**
     * Создание роли
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function createRole(Request $request)
    {
        $data = $request->all();
        $role = Role::create(['name'=>$data['role-name']]);
        return $role;
    }

    /**
     * Создание разрешения
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function createPermission(CreatePermissionRequest $request)
    {
        $data = $request->all();
        $permission = Permission::create(['name' => $data['permission_name']]);
        return response()->json($permission);
    }

    /**
     * Получение списка ролей
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoles()
    {
        $roles = Role::get();
        return response()->json([
            'data' => RoleResource::collection($roles),
            'count' => count($roles),
        ]);
    }

    /**
     * Получение списка разрешений
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermissions()
    {
        $permissions = Permission::all();
        return response()->json([
            'data'=>PermissionsResource::collection($permissions),
            'count'=>count($permissions),
        ]);
    }

    /**
     * Синхронизация ролей
     * @param Request $request
     * @return mixed
     */
    public function setPermissionForRole(Request $request)
    {
        $data = $request->all();
        $role = Role::where('id',$data['role_id'])->first();
        return $role->syncPermissions(explode(',', $data['permission_ids']));
    }

    /**
     * Удаление разрешений для роли
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function removePermissionForRole(Request $request)
    {
        $data = $request->all();
        $role = Role::where('id',$data['role_id'])->first();
        foreach (explode(',', $data['permission_ids']) as $id){
            $role->revokePermissionTo($id);
        }
        return response([]);
    }

    /**
     * Присвоение роли пользователю
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function setRoleForUser(Request $request)
    {
        $data = $request->all();
        if (auth()->user()->hasRole('admin'))
        {
            $user = \App\User::where('id', $data['user_id'])->first();
            $user->assignRole($data['role']);
            return response([]);
        }
    }

    /**
     * Удаление роли у пользователя
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function removeRoleForUser(Request $request)
    {
        $data = $request->all();
        if (auth()->user()->hasRole('admin')){
            $user = \App\User::where('id', $data['user_id'])->first();
            $user->removeRole($data['role-name']);
            return response([]);
        }
    }

    /**
     * Список ролей у конкретного пользователя
     * @return mixed
     */
    public function getAllRolesUser()
    {
        $user = auth()->user();
        return response()->json(['roles'=>$user->getRoleNames()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deletePermission($id)
    {
        Permission::destroy((int)$id);
        return response([]);
    }
}
