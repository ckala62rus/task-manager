<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\CreateProjectRequest;
use App\Http\Resources\ProjectsResource;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    /**
     * Создание проекта для задачника
     * @param CreateProjectRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function addNewProject(CreateProjectRequest $request)
    {
        $data = $request->all();
        $project = new Project();
        $project->slug = $data['slug'];
        $project->description = $data['description'];
        if ($project->save())
        {
            return response('create', 201);
        }
        return response('error create project', 400);
    }

    /**
     * Получение всех проектов списком
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjects()
    {
        $projects = Project::all();
        return response()->json([
            'data'=>ProjectsResource::collection($projects),
            'count'=>count($projects),
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProject($id)
    {
        $project = Project::where('id', $id)->get();
        return $project;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function editProject(Request $request)
    {
        $data = $request->all();
        $project = Project::where('id', $data['project']['id'])->first();
        $project->slug = $data['project']['slug'];
        $project->description = $data['project']['description'];
        $project->save();
        return $data['project']['slug'];
    }

    /**
     * Удаление проекта
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deleteProject($id)
    {
        Project::destroy((int)$id);
        return response([]);
    }
}
