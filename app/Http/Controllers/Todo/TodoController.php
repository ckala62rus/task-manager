<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\StatusTasks;
use App\Models\Task;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     *Создание задачи
     *Удаление задачи
     *Редактирование задачи
     *Изменение статуса задачи
     */

    /**
     * @param TaskRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
//    public function createTask(TaskRequest $request)
    public function createTask(Request $request, $project_id)
    {
        $data = $request->all();
        $task = new Task();
        $task->title = $data['block']['title'];
        $task->description = $data['block']['description'];
        $task->status_id = 1;
        $task->project_id = $project_id;
        if ($task->save())
        {
            return response([],200);
        }
    }

    /**
     * Получение всех задач для конкретного проекта
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTasks($id)
    {
        $tasks = Task::where('project_id', $id)->get();
        return response()->json(['tasks'=>TaskResource::collection($tasks)]);
    }

    /**
     * Обновление конкретной задачи
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateTasks(Request $request, $id)
    {
        $data = $request->all();
        $model = Task::where('id', $id)->first();
        $model->title = $data['block']['title'];
        $model->description = $data['block']['description'];
        $model->save();
        return response([]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneTask(Request $request)
    {
        $data = $request->all();
        $task = Task::where('id', $data['id'])->get();
        return response()->json($task);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateStatusTask(Request $request)
    {
        $data = $request->all();
        $status = StatusTasks::where('status', $data['status'])->first();
        Task::updateOrCreate(
            [
                'id' => $data['id'],
            ],
            [
                'status_id' => $status->id,
            ]);
        return response([]);
    }

    /**
     * @param $id
     */
    public function deleteTask($id)
    {
        Task::destroy((int)$id);
        return response([]);
    }
}
