<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Http\Requests\Status\StatusRequest;
use App\Http\Resources\StatusResource;
use App\Models\StatusTasks;

class StatusTasksController extends Controller
{

    /**
     * Создание статуса
     * @param StatusRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function createStatus(StatusRequest $request)
    {
        $data = $request->all();
        $status = new StatusTasks();
        $status->status = $data['status'];
        if ($status->save())
        {
            return response([],200);
        }
    }

    /**
     * Получение всех статусов
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatuses()
    {
        $statuses = StatusTasks::all();
        return response()->json(['statuses'=>StatusResource::collection($statuses)]);
    }

    /**
     * @param StatusRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateStatus(StatusRequest $request, $id)
    {
        $data = $request->all();
        $status = StatusTasks::where('id', $id)->first();
        $status->status = $data['status'];
        if ($status->save())
        {
            return response([],200);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deleteStatus($id)
    {
        StatusTasks::destroy((int)$id);
        return response([]);
    }
}
