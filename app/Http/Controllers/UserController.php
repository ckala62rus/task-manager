<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Http\Resources\UsersResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws ValidationException
     */
    public function getToken(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
//            'device_name' => 'required'
        ]);

        /** @var User $user */
        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                    'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        $token = $user->createToken('my-app-token')->plainTextToken;

        $response = [
            'user' => UserResource::collection( User::where( 'id', $user->id)->get() ),
            'token' => $token,
        ];

        return response($response, 201);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json( UserResource::collection( User::where( 'id', auth()->user()->id)->get() ) );
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function revokeToken()
    {
        return response(auth()->user()->tokens()->delete(), 201);
    }

    /**
     * Получение списка пользователей
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        $users = User::get();
        return response()->json([
            'data' => UsersResource::collection($users),
            'count' => count($users),
        ]);
    }

    /**
     * Получить одного пользователя
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser($id)
    {
        $user = User::where('id', $id)->get();
        return response()->json(UserResource::collection($user));
    }

    /**
     * Редактирование пользователя
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function editUser(Request $request)
    {
        $data = $request->all();
        $user = User::where('id', $data['user']['id'])->first();
        $user->name = $data['user']['name'];
        $user->email = $data['user']['email'];
        $user->save();
        return response([]);
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        return User::destroy($id);
    }
}
