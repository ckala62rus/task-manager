<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('sanctum/token', 'UserController@getToken');

Route::group([

    'middleware' => 'auth:sanctum',

], function ($router) {

    Route::get('sanctum/me', 'UserController@me');
    Route::post('sanctum/logout', 'UserController@revokeToken');
    Route::post('/test', 'Permission\PermissionController@index');
});

Route::group([

    'middleware' => ['auth.admin','auth:sanctum'],

], function ($router) {

    /******Users******/
    Route::get('/users', 'UserController@getUsers');
    Route::post('/users', 'UserController@editUser');
    Route::post('/users/{id}', 'UserController@getUser');
    Route::delete('/users/{id}', 'UserController@destroy');

    /******Roles******/
    Route::post('/permission/get-all-roles-user', 'Permission\PermissionController@getAllRolesUser');
    Route::post('/permission/remove-role', 'Permission\PermissionController@removeRoleForUser');
    Route::post('/permission/set-role-user', 'Permission\PermissionController@setRoleForUser');
    Route::post('/permission/create-role', 'Permission\PermissionController@createRole');
    Route::post('/permission/create-permission', 'Permission\PermissionController@createPermission');
    Route::post('/permission/set-permission-role', 'Permission\PermissionController@setPermissionForRole');
    Route::get('/permission/get-roles', 'Permission\PermissionController@getRoles');
    Route::get('/permission/get-permissions', 'Permission\PermissionController@getPermissions');
    Route::post('/permission/remove-permissions-role', 'Permission\PermissionController@removePermissionForRole');
    Route::delete('/permission/remove-permissions/{id}', 'Permission\PermissionController@deletePermission');

    /******Todo-part******/
    Route::post('/task/create/{project_id}', 'Todo\TodoController@createTask');
    Route::post('/task/one-task', 'Todo\TodoController@getOneTask');
    Route::post('/task/update-status', 'Todo\TodoController@updateStatusTask');
    Route::get('/task/{id}', 'Todo\TodoController@getTasks');
    Route::post('/task/{id}', 'Todo\TodoController@updateTasks');
    Route::delete('/task/{id}', 'Todo\TodoController@deleteTask');

    /******Projects******/
    Route::get('/project', 'Todo\ProjectController@getProjects');
    Route::post('/project', 'Todo\ProjectController@addNewProject');
    Route::post('/project/edit', 'Todo\ProjectController@editProject');
    Route::post('/project/{id}', 'Todo\ProjectController@getProject');
    Route::delete('/project/{id}', 'Todo\ProjectController@deleteProject');

    /******Columns status for tasks******/
    Route::post('/status', 'Todo\StatusTasksController@createStatus');
    Route::get('/status', 'Todo\StatusTasksController@getStatuses');
    Route::get('/status/{id}', 'Todo\StatusTasksController@updateStatus');
    Route::delete('/status/{id}', 'Todo\StatusTasksController@deleteStatus');

});
