<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Role::create(['name' => 'admin']);
//        Permission::create(['name' => 'admin']);
        $user = \App\User::where('name', 'Admin')->first();
//        dd($user);
        $user->assignRole('admin');
    }
}
